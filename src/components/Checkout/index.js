import React, { useEffect, useState } from "react";
import CardDetails from "./CardDetails.js";
import { Elements } from "@stripe/react-stripe-js";

const Checkout = ({
  style,
  chargeAmount = 0,
  chargeAppearsAs = "Stripe",
  chargeCurrency = "USD",
  chargeCurrencySymbol = "$",
  chargeCompletionMessage = "Thank You",
  title = "Checkout",
  subTitle,
  chargeReceiptEmail,
  onComplete,
  stripeKey,
  stripeHelper,
  onSuccess,
  token,
  product,
  stripePromise,
  metadata
}) => {
  const styles = {
    wrapper: {
      display: "flex",
      alignItems: "center",
      ...style?.wrapper
    },
    container: {
      width: "100%",
      ...style?.container
    },
    title: {
      textAlign: "left",
      fontWeight: 900,
      fontSize: "2rem",
      ...style?.title
    },
    subTitle: {
      textAlign: "left",
      ...style?.subtitle
    },
    charge: {
      fontWeight: "900",
      fontSize: "2rem",
      textAlign: "center",
      margin: "1rem",
      ...style?.charge
    },
    paymentButton: {
      textAlign: "center",
      margin: "1.2rem 1rem 1rem 1rem"
    },
    chargeFormContainer: {
      ...style?.chargeFormContainer
    },
    chargeAppearsAs: { fontWeight: 900, ...style?.chargeAppearsAs },
    chargeReceiptEmail: { fontWeight: 900, ...style?.chargeReceiptEmail },
    chargeCompletionMessage: { textAlign: "center", marginTop: "1rem" },
    footer: {
      textAlign: "center",
      marginTop: "1.5rem"
    },
    error: {
      position: "relative",
      width: "100%",
      minHeight: "2rem",
      background: "#c34c4c",
      top: 0,
      left: 0,
      margin: 0,
      display: "flex",
      alignItems: "center",
      fontSize: "1.5rem",
      color: "white",
      ...style.error
    },
    inputContainer: { marginRight: 0, color: "#4ebd05" },
    cardElement: {
      base: {
        fontSize: "20px",
        color: "#4ebd05",
        "::placeholder": {
          color: "#aaaaaa",
          fontSize: "20px"
        },
        ...(style.cardElement?.base ? style.cardElement?.base : {})
      },
      invalid: {
        color: "#9e2146",
        ...(style.cardElement?.invalid ? style.cardElement?.invalid : {})
      },
      ...(style.cardElement ? style.cardElement : {})
    },
    row: {
      margin: "auto",
      display: "flex",
      flexDirection: "row",
      alignItems: "center"
    },
    button: {
      marginRight: ".5rem",
      ...style?.button
    }
  };

  const [receiptUrl, setReceiptUrl] = useState();
  const [cardError, setCardError] = useState();
  const onCardError = (err) => {
    console.error(err);
    setCardError(err);
  };

  const paymentDisabled =
    isNaN(chargeAmount) || chargeAmount < 0 ? true : false;

  if (!stripePromise || !stripeHelper) {
    console.error("Checkout: Missing stripe promise");
    return null;
  }

  chargeAmount = chargeAmount ? chargeAmount : 0;
  const isFree = chargeAmount === 0;

  if (receiptUrl) {
    return (
      <div style={styles.wrapper}>
        <div style={styles.container}>
          <div style={styles.title}>Payment Successful!</div>
          <div style={styles.chargeFormContainer}>
            {chargeAppearsAs && (
              <div style={styles.chargeAppearsAs}>
                Charge will appear as "{chargeAppearsAs}"
              </div>
            )}
            {chargeCompletionMessage && (
              <div style={styles.chargeCompletionMessage}>
                {chargeCompletionMessage}
              </div>
            )}
            <div style={{ display: "flex" }}>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  margin: "auto"
                }}
              >
                <div style={{ display: "flex", marginTop: "2rem" }}>
                  <button
                    disabled={paymentDisabled}
                    style={styles.button}
                    onClick={() => window.open(receiptUrl, "_blank")}
                  >
                    <div style={{ display: "flex" }}>
                      <div style={styles.row}>
                        <div>View Receipt</div>
                        <div style={{ padding: "0 0 0 .5rem" }}>
                          <div className="fas fa-receipt" />
                        </div>
                      </div>
                    </div>
                  </button>
                  {onComplete && (
                    <button
                      disabled={paymentDisabled}
                      style={styles.button}
                      onClick={onComplete}
                    >
                      Ok
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div style={styles.wrapper}>
      <div style={styles.container}>
        {cardError && (
          <div style={styles.error}>
            <div style={{ margin: "0 .5rem" }}>
              <span className="fas fa-exclamation-circle" />
            </div>
            <div style={{ flexGrow: 1 }}>
              {cardError.message
                ? cardError.message
                : `${cardError.status}: ${cardError.statusText}`}
            </div>
          </div>
        )}

        <div style={styles.title}>{title}</div>
        {subTitle && <div style={styles.subTitle}>{subTitle}</div>}

        <div style={styles.chargeFormContainer}>
          <div style={styles.charge}>
            {chargeCurrencySymbol}
            {chargeAmount} {chargeCurrency}
          </div>
          <div id="payment-request-button" style={{ margin: "0,10px" }}></div>
          <div
            id="paymentbuttons"
            style={{ opacity: paymentDisabled ? 0.5 : 1 }}
          ></div>

          <Elements stripe={stripePromise}>
            <CardDetails
              metadata={metadata}
              isFree={isFree}
              token={token}
              onSuccess={onSuccess}
              stripeKey={stripeKey}
              stripeHelper={stripeHelper}
              styles={styles}
              description={product.name}
              paymentDisabled={paymentDisabled}
              currency={chargeCurrency}
              chargeCurrencySymbol={chargeCurrencySymbol}
              chargeAmount={chargeAmount}
              chargeReceiptEmail={chargeReceiptEmail}
              product={product}
              onCardError={onCardError}
              onCardSuccess={(data) => {
                setReceiptUrl(data.charge.receipt_url);
                if (typeof onSuccess === "function") onSuccess(data);
              }}
            />
          </Elements>
          {!isFree && (
            <div style={styles.footer}>
              <div style={styles.chargeAppearsAs}>
                Charge will appear as "{chargeAppearsAs}"
              </div>
              {chargeReceiptEmail && (
                <div style={styles.chargeReceiptEmail}>
                  Receipt will be sent to {chargeReceiptEmail}
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Checkout;
