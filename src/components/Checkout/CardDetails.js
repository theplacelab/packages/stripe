import React, { useState } from "react";
import { CardElement } from "@stripe/react-stripe-js";
import { useStripe, useElements } from "@stripe/react-stripe-js";
import { Spinner } from "/src/components/Spinner";

const CardDetails = ({
  isFree,
  token,
  product,
  onCardError,
  onCardSuccess,
  styles,
  paymentDisabled,
  onSubmit,
  currency,
  chargeCurrencySymbol,
  stripeKey,
  stripeHelper,
  description = "PURCHASE",
  chargeAmount = 0,
  chargeReceiptEmail,
  metadata
}) => {
  const stripe = useStripe();
  const elements = useElements();
  const [showSpinner, setShowSpinner] = useState(false);

  if (!product) return null;

  const handleSubmit = (event) => {
    event.preventDefault();
    setShowSpinner(true);
    const afterToken = (result) => {
      if (result.error) {
        onCardError(result.error);
        setShowSpinner(false);
      } else {
        // Ensure amount is in cents
        const amount = chargeAmount * 100;
        const body = {
          amount,
          source: result.token.id,
          receipt_email: chargeReceiptEmail,
          product_id: product.id,
          description,
          token,
          metadata
        };
        try {
          fetch(`${stripeHelper}/charge`, {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`
            },
            method: "POST",
            body: JSON.stringify(body)
          }).then((response) => {
            setShowSpinner(false);
            if (response.ok) {
              response.json().then((data) => {
                if (!data.charge) {
                  onCardError(data);
                } else {
                  onCardSuccess(data);
                }
              });
            } else {
              onCardError(response);
            }
          });
        } catch (error) {
          debugger;
          setShowSpinner(false);
          onCardError(error);
          console.error(error);
        }
      }
    };
    if (isFree) {
      afterToken({ token: { id: "free" } });
    } else {
      stripe.createToken(elements.getElement(CardElement)).then(afterToken);
    }
  };
  const paymentButtonText = isFree
    ? "Free (Complete Purchase)"
    : `Pay ${chargeCurrencySymbol} ${chargeAmount} ${currency}`;

  return (
    <div>
      <div className="checkout-form">
        {showSpinner && <Spinner />}
        <form onSubmit={handleSubmit}>
          {!isFree && (
            <label>
              <div className="input-text" style={styles.cardElement}>
                <CardElement
                  options={{
                    style: styles.cardElement
                  }}
                />
              </div>
            </label>
          )}
          <div style={styles.inputLabel}>
            <div style={styles.paymentButton}>
              <button
                disabled={paymentDisabled}
                style={styles.button}
                id="stripebutton"
              >
                <div style={{ display: "flex" }}>
                  <div
                    style={{
                      margin: "auto",
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <div onClick={onSubmit}>{paymentButtonText}</div>
                    {!isFree && (
                      <span
                        style={{ marginLeft: ".5rem" }}
                        className="fas fa-credit-card"
                      />
                    )}
                  </div>
                </div>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CardDetails;
