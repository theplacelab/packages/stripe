import React from "react";
import { Ring } from "react-spinners-css";
export const Spinner = () => {
  return (
    <div
      style={{
        position: "absolute",
        backgroundColor: "white",
        height: "100%",
        width: "100%",
        zIndex: 2,
        opacity: 0.9,
        display: "flex",
        top: 0,
        left: 0
      }}
    >
      <div
        style={{
          width: "100%",
          margin: "auto",
          textAlign: "center",
          fontWeight: 900,
          fontSize: "1.5rem"
        }}
      >
        Processing...
        <Ring
          color="#7a7676"
          size={32}
          style={{
            margin: "auto",
            position: "relative",
            zIndex: 2,
            color: "black",
            border: "0px solid blue"
          }}
        />
      </div>
    </div>
  );
};
