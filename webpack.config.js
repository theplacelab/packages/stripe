var path = require("path");
const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const WebpackShellPlugin = require("webpack-shell-plugin");

module.exports = (env, argv) => {
  const mode = argv.mode;
  const postBuildScript =
    process.env.YALC === "true" ? "npx yalc push" : "true";
  let plugins = argv.addon === "analyze" ? [new BundleAnalyzerPlugin()] : [];
  plugins.push(new WebpackShellPlugin({ onBuildExit: postBuildScript }));

  console.log(`Environment: ${mode}`);
  return {
    plugins,
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "[name].js",
      library: "index",
      globalObject: "this",
      libraryTarget: "commonjs2",
    },
    externals: {
      webpack: "commonjs webpack",
      "@babel": "commonjs @babel",
      react: "commonjs react",
      "prop-types": "commonjs prop-types",
      "@fortawesome/react-fontawesome":
        "commonjs @fortawesome/react-fontawesome",
      "@fortawesome/free-solid-svg-icons":
        "commonjs @fortawesome/free-solid-svg-icons",
      "@fortawesome/fontawesome-svg-core":
        "commonjs @fortawesome/fontawesome-svg-core",
      "react-redux": "commonjs react-redux",
      "react-dom": "commonjs react-dom",
      "react-router-dom": "commonjs react-router-dom",
      "redux-saga": "commonjs redux-saga",
      "@semantic-release/git": "commonjs semantic-release/git",
      "@semantic-release/gitlab": "commonjs semantic-release/gitlab",
      "semantic-release": "commonjs semantic-release",
      "core-js": "commonjs core-js",
      "@stripe/react-stripe-js": "commonjs @stripe/react-stripe-js",
      "@stripe/stripe-js": "commonjs @stripe/stripe-js",
    },
    devtool: mode === "development" ? "inline-source-map" : false,
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.join(__dirname, "src"),
          exclude: path.join(__dirname, "/node_modules/"),
          loader: "babel-loader",
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
      ],
    },
  };
};
