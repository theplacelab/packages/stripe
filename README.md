# Package: @theplacelab/stripe

React components for Stripe Payment, integrates with [Stripe Service](https://gitlab.com/theplacelab/service/stripe)

[[README: Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)]  
[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/stripe/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/stripe/-/pipelines/latest)

# In This Package:

## `<Checkout/>`

```js
<Checkout
  stripeKey={publicApiKey}
  style={{ button: defaultStyles.button }}
  title="Checkout"
  subTitle="Thanks for your money"
  chargeAmount="1234.56"
  chargeAppearsAs="Charging Company"
  chargeReceiptEmail="buyer@example.com"
  onComplete={() => console.log("Complete!")}
/>
```

# Development

Use [yalc](https://github.com/wclr/yalc) to [solve issues with link](https://divotion.com/blog/yalc-npm-link-alternative-that-does-work). Install yalc, then:

In this package:

```
yalc publish
yalc push
```

Where you want to use it:

```
yalc link @theplacelab/stripe
```

# How to Use

- Get a Personal Access Token from Gitlab

- Add an `.npmrc` to your project using the token in place of ~GITLAB_TOKEN~ (or alternately add this to your ~/.npmrc or use config to do that)

  ```
  @theplacelab:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken="~GITLAB_TOKEN~"
  ```

  or

  ```
  npm config set @theplacelab:registry https://gitlab.com/api/v4/packages/npm/
  npm config set //gitlab.com/api/v4/packages/npm/:_authToken '~GITLAB_TOKEN~'
  ```

- `yarn add @theplacelab/stripe`
